<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"> 
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.2.min.js"></script>
<style>
body {
    background-color: black;
    
}
.cc{
     cursor:url('icon.png'), auto;
    background-image:url('red_cat.jpg');
    position: fixed;
    height: 100%;
    width: 100%;
    left:0;
    top:0;
}
	@-webkit-keyframes fadeIn {
    0% {opacity: 0; /*初始状态 透明度为0*/}
    10%{opacity: 0.1;}
    20%{opacity: 0.2;}
    30%{opacity: 0.3;}
    40%{opacity: 0.4;}
    50% {opacity: 0.5; /*中间状态 透明度为0*/}
    70%{opacity: 0.7;}
    80%{opacity: 0.8;}
    90%{opacity: 0.9;}
    100% {opacity: 1; /*结尾状态 透明度为1*/}
	}

	@-webkit-keyframes fadeOut{
		0% {opacity: 0; /*初始状态 透明度为0*/}
    	10%{opacity: 0.1;}
    	20%{opacity: 0.2;}
    	30%{opacity: 0.3;}
    	40%{opacity: 0.4;}
    	50% {opacity: 0.7; /*中间状态 透明度为0*/}
    	70%{opacity: 0.4;}
    	80%{opacity: 0.3;}
    	90%{opacity: 0.2;}
    	100% {opacity: 0; /*结尾状态 透明度为1*/}
	}

p.title{
	font-family: "Arial","Sans-serif","Monospace";font-size: 65px;color:silver;letter-spacing: 0.1cm;position: absolute;

	-webkit-animation-name: fadeIn; 
    -webkit-animation-duration: 1s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 
}
p.subtitle{
	font-family: "Arial","Sans-serif","Monospace";font-size: 80px;color:gray;letter-spacing: 0.2cm;position: absolute;margin-top: 5.5cm;margin-left: 6.5cm;
	text-shadow: 9px 6px 5px black;
	-webkit-animation-name: fadeOut; 
    -webkit-animation-duration: 2s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 

}
p.time{
	font-family: "Arial","Sans-serif","Monospace";font-size: 25px;color:black;letter-spacing: 0.15cm; margin-top: 5.5cm;margin-left: 2cm;
	text-shadow:3px 3px 5px silver;
}
p.enter{
	font-family: "Verdana","Sans-serif","Monospace";font-size: 30px;color:black;letter-spacing: 0.15cm;border: 1px solid black;text-transform: uppercase; 
	width: 150px;padding:0.2cm;
	-moz-border-radius: 50px;
	-webkit-border-radius: 50px;
	border-radius: 50px;;
	text-align: center;position: fixed;
	-webkit-animation-name: fadeIn; 
    -webkit-animation-duration: 1s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 
}
</style>
<div class="cc"></div>
</head>