<?php
include ('template.php');
?>
<html>
<head>
<meta charset="utf-8">
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.2.min.js"></script>
<style>
  @-webkit-keyframes fadeIn {
    0% {opacity: 0; /*初始状态 透明度为0*/}
    10%{opacity: 0.1;}
    20%{opacity: 0.2;}
    30%{opacity: 0.3;}
    40%{opacity: 0.4;}
    50% {opacity: 0.5; /*中间状态 透明度为0*/}
    70%{opacity: 0.7;}
    80%{opacity: 0.8;}
    90%{opacity: 0.9;}
    100% {opacity: 0.9; /*结尾状态 透明度为1*/}
  }

p.story{
	display: inline;font-family: "Arial","Sans-serif","Monospace";font-size: 30px;color:black;visibility: hidden;opacity: 0.9;
	    letter-spacing: 0.02cm;text-align: left;font-variant: small-caps;position: absolute;text-shadow:1px 1px 5px black;
	 -webkit-animation-name: fadeIn; 
    -webkit-animation-duration: 3s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 
}
p.mid{
	font-family: "Arial","Sans-serif","Monospace";font-size: 15px;color:black;letter-spacing: 0.1cm;
	text-align: center;font-variant: small-caps;position: absolute;text-shadow:1px 1px 5px black;
	 -webkit-animation-name: fadeIn; 
    -webkit-animation-duration: 2.7s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 
}
#pretty,#good,#sad,#rain{
	position: absolute;opacity: 0.9;margin-top: 10.5cm;width:5cm;margin-left: 12cm;
	 -webkit-animation-name: fadeIn; 
    -webkit-animation-duration: 0.3s; 
    -webkit-animation-iteration-count: 1; 
    -webkit-animation-delay: 0s; 
}
p.mood{
	font-family: "Arial","Sans-serif","Monospace";font-size: 13px;color:black;letter-spacing: 0.1cm;opacity: 0.8;
	text-align: center;font-variant: small-caps;position: absolute;text-shadow:1px 1px 5px black;visibility: hidden;
}
</style>

</head>

<body>
<img src="taoshu.gif" style="position:absolute;opacity:0.8; width:19cm" id="huaban">
<img src="rose.gif" style="position:absolute;opacity:0.7; width:1cm;height:2cm;margin-left:25.5cm;margin-top:9.5cm">
<img src="pot.gif" style="position:absolute;opacity:0.9; width:12cm;height:6cm;margin-left:22.5cm;margin-top:14cm">
<img src="hang.png" style="position:absolute;width:7cm;height:10cm" id="hang">
<img src="ground.png" style="position:absolute;width:11cm;height:11cm" id="ground">
<img src="walk.png" style="position:absolute" id="walk">
<img src="sit_2.png" style="position:absolute" id="sit2">

<a href="memory.php">
<p class="story">s<br><br><br>fo</p>
<p class="story">t<br><br><br>ot</p>
<p class="story">o<br><br><br>pr</p>
<p class="story">r<br><br><br>ints</p>
<p class="story">y<br><br><br></p></a>
<p class="mid" id="mid" style="display:none">see our story and follow by footprints<br>
BiuBiu with MeowMeow<br></p>
<a href="#!"><p class="mid" id="share" style="display:none" onclick="share()">share mood</p></a>
<p class="mid" id="note" style="display:none;margin-top:10.5cm;margin-left:18cm">notice sent</p>
<a href="#!"><img src="rain.gif" id="rain" style="opacity:1;margin-left:21cm;display:none;"></a>
<a href="#!"><img src="pretty.gif" id="pretty" style="display:none" onmouseover="over(this)" onmouseout="out(this)" onclick="hide(this)"></a>
<a href="#!"><img src="good.gif" id="good" style="display:none" onmouseover="over(this)" onmouseout="out(this)" onclick="hide(this)"></a>

<iframe id="myframe" name="myframe" style="display:none"></iframe>
<form method="post" action="story.php" id="myform" target="myframe">
<a href="#!"><img src="sad.gif" id="sad" onmouseover="over(this)" onmouseout="out(this)" style="display:none;" onclick="hide(this)"></a>
<input type="text" name="sharemood" style="visibility:hidden" value="sad" id="input">
</form>
<p class="mood" style="margin-left:22cm;margin-top:10.5cm">sad</p>
<p class="mood" style="margin-left:18cm;margin-top:13.5cm">good</p>
<p class="mood" style="margin-left:11.5cm;margin-top:10.5cm">superb</p>
<script>
document.getElementById("footer").style.marginTop="19cm";
setTimeout(function(){
	document.getElementById("mid").style.display='';
	document.getElementById("mid").style.marginLeft="14cm";
	document.getElementById("mid").style.marginTop="7.9cm";
	document.getElementById("share").style.display='';
	document.getElementById("share").style.marginTop="10.5cm";
	document.getElementById("share").style.marginLeft="18cm";
},2000);

document.getElementById("huaban").style.marginLeft="16.2cm";
document.getElementById("huaban").style.marginTop="1cm";
document.getElementById("hang").style.marginTop="0.4cm";
document.getElementById("hang").style.marginLeft="4cm";
document.getElementById("walk").style.marginTop="12cm";
document.getElementById("sit2").style.marginTop="12cm";
document.getElementById("walk").style.marginLeft="4cm";
document.getElementById("sit2").style.marginLeft="11cm";
document.getElementById("ground").style.marginTop="9cm";
document.getElementById("good").style.marginLeft="16cm";
document.getElementById("sad").style.marginTop="11.5cm";
document.getElementById("sad").style.marginLeft="21cm";
document.getElementById("pretty").style.marginTop="10cm";
var arr=document.getElementsByClassName("story");
var mood=document.getElementsByClassName("mood");
var width=window.innerWidth;
var time=400;
for(var i=0;i<=arr.length;i++)
	{
		var left=i*30+(width/2)+"px";
		arr[i].style.marginTop="6.5cm";
		arr[i].style.marginLeft=left;
		setTimeout(function(){arr[0].style.visibility="visible";});
		setTimeout(function(){arr[1].style.visibility="visible";},time);
		setTimeout(function(){arr[2].style.visibility="visible";},time*2);
		setTimeout(function(){arr[3].style.visibility="visible";},time*3);	
		setTimeout(function(){arr[4].style.visibility="visible";},time*4);
	}
function share()
{
	document.getElementById("pretty").style.display='';
	document.getElementById("good").style.display='';
	document.getElementById("sad").style.display='';
}
function over(obj)
{	
	if(obj.id=="pretty"){obj.src="dance.gif";mood[2].style.visibility="visible";}
	if(obj.id=="sad"){document.getElementById("rain").style.display='';
	mood[0].style.visibility="visible";}
	if(obj.id=="good")mood[1].style.visibility="visible";
}
function out(obj)
{
	if(obj.id=="pretty"){obj.src="pretty.gif";mood[2].style.visibility="hidden";}
	if(obj.id=="sad") {document.getElementById("rain").style.display="none";mood[0].style.visibility="hidden";}
	if(obj.id=="good")mood[1].style.visibility="hidden";
}
function hide(obj)
{
	if(obj.id=="pretty")document.getElementById("input").value="pretty";
	if(obj.id=="good")document.getElementById("input").value=" ";
	if(obj.id=="sad")document.getElementById("input").value="sad";
	document.getElementById("myform").submit();
	for(var k=0;k< mood.length;k++)
		{mood[k].style.display="none";}
	document.getElementById("rain").style.display="none";
	document.getElementById("good").style.display="none";
	document.getElementById("sad").style.display="none";
	document.getElementById("pretty").style.display="none";
	document.getElementById("share").style.display="none";
	
	document.getElementById("note").style.display='';
	
	setTimeout(function(){document.getElementById("note").style.display="none";
	document.getElementById("share").style.display='';
	},2500);
	
}
</script>
<?php
if($_POST['sharemood']=="sad")$message="Alert! Your girlfriend is not happy!";
if($_POST['sharemood']=="pretty")$message="Your girlfriend is missing you";
 require 'PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    $mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'zhouyi910821@gmail.com';                 // SMTP username
$mail->Password = 'fybgG7rm';                           // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 465;                                   // TCP port to connect to


$mail->From ='zhouyi910821@gmail.com';
$mail->FromName ='meow';
$mail->addAddress('7653378617@txt.att.net');     // Add a recipient             // Name is optional
$mail->addReplyTo('zhouyi910821@gmail.com');
$mail->isHTML(true);  
$mail->Body =$message;
	$mail->send();

?>
</body>
</html>